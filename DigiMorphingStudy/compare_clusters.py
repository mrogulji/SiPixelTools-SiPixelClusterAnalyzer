import ROOT

def createRatio(h1, h2):

     h3 = h2.Clone("h3")
#     print "h3: ",h3
     h3.SetLineColor(ROOT.kBlack)
     h3.SetMarkerStyle(21)
     h3.SetTitle("")
     # Set up plot for markers and errors
     h3.Sumw2()
     h3.SetStats(0)
     h3.Divide(h1)
     #h3.SetMinimum(h3.GetMinimum()*0.6)
     h3.SetMinimum(0.9)
     #h3.SetMaximum(h3.GetMaximum()*1.4)
     h3.SetMaximum(1.1)

     # Adjust y-axis settings
     y = h3.GetYaxis()
     y.SetTitle("Morphed/Nominal")
     y.SetNdivisions(505)
     y.SetTitleSize(20)
     y.SetTitleFont(43)
     y.SetTitleOffset(1.55)
     y.SetLabelFont(43)
     y.SetLabelSize(15)

     # Adjust x-axis settings
     x = h3.GetXaxis()
     x.SetTitleSize(20)
     x.SetTitleFont(43)
     x.SetTitleOffset(4.0)
     x.SetLabelFont(43)
     x.SetLabelSize(15)

     return h3

def createCanvasPads(c_name):
     c = ROOT.TCanvas(c_name, c_name, 800, 800)
     # Upper histogram plot is pad1
     pad1 = ROOT.TPad("pad1"+c_name, "pad1"+c_name, 0, 0.3, 1, 1.0)
     pad1.SetBottomMargin(0)  # joins upper and lower plot
     pad1.SetGridx()
     pad1.SetGridy()
     pad1.Draw()
     # Lower ratio plot is pad2
     c.cd()  # returns to main canvas before defining pad2
     pad2 = ROOT.TPad("pad2"+c_name, "pad2"+c_name, 0, 0.05, 1, 0.3)
     pad2.SetTopMargin(0)  # joins upper and lower plot
     pad2.SetBottomMargin(0.2)
     pad2.SetGridx()
     pad2.SetGridy()
     pad2.Draw()

     return c, pad1, pad2

histlist = [
'h2_Cluster_Z_vs_phi_BPixL1',
'h2_Cluster_Z_vs_phi_BPixL2',
'h2_Cluster_Z_vs_phi_BPixL3',
'h2_Cluster_Z_vs_phi_BPixL4',
#'h2_Cluster_Z_vs_phi_FPixD1',
#'h2_Cluster_Z_vs_phi_FPixD2',
#'h2_Cluster_Z_vs_phi_FPixD3',
#'h2_Cluster_Z_vs_phi_BPixL1_merged',
#'h2_Cluster_Z_vs_phi_BPixL2_merged',
#'h2_Cluster_Z_vs_phi_BPixL3_merged',
#'h2_Cluster_Z_vs_phi_BPixL4_merged',
#'h2_Cluster_Z_vs_phi_FPixD1_merged',
#'h2_Cluster_Z_vs_phi_FPixD2_merged',
#'h2_Cluster_Z_vs_phi_FPixD3_merged',
#'h2_Cluster_Z_vs_phi_BPixL1_ratio',
#'h2_Cluster_Z_vs_phi_BPixL2_ratio',
#'h2_Cluster_Z_vs_phi_BPixL3_ratio',
#'h2_Cluster_Z_vs_phi_BPixL4_ratio',
#'h2_Cluster_Z_vs_phi_FPixD1_ratio',
#'h2_Cluster_Z_vs_phi_FPixD2_ratio',
#'h2_Cluster_Z_vs_phi_FPixD3_ratio'
]


file_morph = ROOT.TFile("cluster_histograms_morph.root")
file_nomorph = ROOT.TFile("cluster_histograms_nomorph.root")

morphhist_z_ntracks = file_morph.Get("h2_Cluster_Z_vs_ntracks_BPixL1")
nomorphhist_z_ntracks = file_nomorph.Get("h2_Cluster_Z_vs_ntracks_BPixL1")

morphhist_z_ntracks.RebinX(10)
nomorphhist_z_ntracks.RebinX(10)
morphhist_z_ntracks.GetYaxis().SetRangeUser(1, 6)
nomorphhist_z_ntracks.GetYaxis().SetRangeUser(1, 6)

print morphhist_z_ntracks.GetNbinsY()
print nomorphhist_z_ntracks.GetNbinsY()

ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPaintTextFormat('1.2f')
ct = ROOT.TCanvas("ct","ct", 800, 800)
morphhist_z_ntracks.Draw("colz")
ROOT.gPad.Update()
ROOT.gPad.SetLogz()
ct.SaveAs("Z_vs_ntracks_BPixL1_morph.png")
nomorphhist_z_ntracks.Draw("colz")
ROOT.gPad.Update()
ROOT.gPad.SetLogz()
ct.SaveAs("Z_vs_ntracks_BPixL1_nomorph.png")
morphhist_z_ntracks_ratio = morphhist_z_ntracks.Clone("morph_z_ntracks_ratio")
morphhist_z_ntracks_ratio.Divide(nomorphhist_z_ntracks)
#morphhist_z_ntracks_ratio.SetMinimum(0.9)
#morphhist_z_ntracks_ratio.SetMaximum(1.0)

morphhist_z_ntracks_ratio.Draw("colztext")
ROOT.gPad.Update()
ROOT.gPad.SetLogz(0)
ct.SaveAs("Z_vs_ntracks_BPixL1_morph_ratio.png")

xbin_start = morphhist_z_ntracks.FindBin(-30.0)
xbin_end = morphhist_z_ntracks.FindBin(-20.0)

print xbin_start
print xbin_end
ntracks_morph   = morphhist_z_ntracks.ProjectionY("ntracks_morph",xbin_start,xbin_end)
ntracks_nomorph = nomorphhist_z_ntracks.ProjectionY("ntracks_nomorph",xbin_start,xbin_end)

xbin_start = morphhist_z_ntracks.FindBin(20.0)
xbin_end = morphhist_z_ntracks.FindBin(30.0)

print xbin_start
print xbin_end
ntracks_morph.Add(morphhist_z_ntracks.ProjectionY("ntracks_morph2",xbin_start,xbin_end))
ntracks_nomorph.Add(nomorphhist_z_ntracks.ProjectionY("ntracks_nomorph2",xbin_start,xbin_end))

print ntracks_morph.GetBinContent(1)
print ntracks_nomorph.GetBinContent(1)

print ntracks_morph

#ntracks_morph.SetAxisRange(2.0,5.0)
#ntracks_nomorph.SetAxisRange(2.0,5.0)

ntracks_morph.SetLineColor(ROOT.kRed)
ntracks_nomorph.SetLineColor(ROOT.kBlue)
ntracks_morph.SetLineWidth(3)
ntracks_nomorph.SetLineWidth(3)
ratio_tracks = createRatio(ntracks_nomorph,ntracks_morph)

l = ROOT.TLegend(0.7,0.7,0.9,0.9)
l.AddEntry(ntracks_morph,"Morphing","L")
l.AddEntry(ntracks_nomorph,"w/o Morphing","L")

ctrack, trpad1, trpad2 = createCanvasPads("ctr")

trpad1.cd()
ntracks_morph.Draw("hist")
ntracks_nomorph.Draw("histsame")
ROOT.gPad.SetLogy()
ROOT.gPad.Update()
l.Draw()

trpad2.cd()
ratio_tracks.Draw("EP")
#ROOT.gPad.Update()
#ROOT.gPad.SetLogy()
ctrack.SaveAs("ntracks_BPixL1_edgeZ.png")

xbin_start = morphhist_z_ntracks.FindBin(-8.0)
xbin_end = morphhist_z_ntracks.FindBin(7.0)

print xbin_start
print xbin_end
ntracks_morph_center   = morphhist_z_ntracks.ProjectionY("ntracks_morph_cen",xbin_start,xbin_end)
ntracks_nomorph_center = nomorphhist_z_ntracks.ProjectionY("ntracks_nomorph_cen",xbin_start,xbin_end)

print "No Morphed single clus tracks: "+str(ntracks_nomorph_center.GetBinContent(1))
print "Morphed single clus tracks: "+str(ntracks_morph_center.GetBinContent(1))+"\n"

print "No Morphed multiple clus tracks: "+str(ntracks_nomorph_center.Integral(2,30))
print "Morphed multiple clus tracks: "+str(ntracks_morph_center.Integral(2,30))

ntracks_morph_center.SetLineColor(ROOT.kRed)
ntracks_nomorph_center.SetLineColor(ROOT.kBlue)
ntracks_morph_center.SetLineWidth(3)
ntracks_nomorph_center.SetLineWidth(3)
ratio_tracks_center = createRatio(ntracks_nomorph_center,ntracks_morph_center)

#l = ROOT.TLegend(0.7,0.7,0.9,0.9)
#l.AddEntry(ntracks_morph_center,"Morphing","L")
#l.AddEntry(ntracks_nomorph_center,"w/o Morphing","L")

ctrack_center, trpad1_center, trpad2_center = createCanvasPads("ctr_center")

trpad1_center.cd()
ntracks_morph_center.Draw("hist")
ntracks_nomorph_center.Draw("histsame")
ROOT.gPad.SetLogy()
ROOT.gPad.Update()
l.Draw()

trpad2_center.cd()
ratio_tracks_center.Draw("EP")
#ROOT.gPad.Update()
#ROOT.gPad.SetLogy()
ctrack_center.SaveAs("ntracks_BPixL1_centerZ.png")
ROOT.gPad.SetLogy(0)


morphhist_npix = file_morph.Get("h1_Cluster_Z_vs_nPixels_BPixL1")
nomorphhist_npix = file_nomorph.Get("h1_Cluster_Z_vs_nPixels_BPixL1")

ratio_npix = createRatio(nomorphhist_npix,morphhist_npix)

c_npix,pad1_npix,pad2_npix = createCanvasPads("c_npix")

morphhist_npix.SetLineColor(ROOT.kRed)
nomorphhist_npix.SetLineColor(ROOT.kBlue)
morphhist_npix.SetLineWidth(3)
nomorphhist_npix.SetLineWidth(3)
pad1_npix.cd()
morphhist_npix.Draw("hist")
nomorphhist_npix.Draw("histsame")
l.Draw()
pad2_npix.cd()
ratio_npix.Draw("EP")
c_npix.SaveAs("nPixel_Z_BPixL1.png")

morphhist_ADC = file_morph.Get("h1_Pixel_ADC_BPixL1")
nomorphhist_ADC = file_nomorph.Get("h1_Pixel_ADC_BPixL1")

ratio_ADC = createRatio(nomorphhist_ADC,morphhist_ADC)

c_ADC,pad1_ADC,pad2_ADC = createCanvasPads("c_ADC")

morphhist_ADC.SetLineColor(ROOT.kRed)
nomorphhist_ADC.SetLineColor(ROOT.kBlue)
morphhist_ADC.SetLineWidth(3)
nomorphhist_ADC.SetLineWidth(3)
pad1_ADC.cd()
morphhist_ADC.Draw("hist")
nomorphhist_ADC.Draw("same")
l.Draw()
pad2_ADC.cd()
ratio_ADC.Draw("EP")
c_ADC.SaveAs("Pixel_ADC_BPixL1.png")
for hist in histlist:

    morphhist = file_morph.Get(hist)
    morphhist_merged = file_morph.Get(hist+"_merged")

    nomorphhist = file_nomorph.Get(hist)
    nomorphhist_merged = file_nomorph.Get(hist+"_merged")

    morphhistZ = morphhist.ProjectionX("morphZ")
    morphhist_mergedZ = morphhist_merged.ProjectionX("morph_mergedZ")

    morphhist_ratio = morphhist_mergedZ.Clone("morph_ratio")
    morphhist_ratio.Divide(morphhistZ)

    nomorphhistZ = nomorphhist.ProjectionX("nomorphZ")
    nomorphhist_mergedZ = nomorphhist_merged.ProjectionX("nomorph_mergedZ")

    nomorphhist_ratio = nomorphhist_mergedZ.Clone("nomorph_ratio")
    nomorphhist_ratio.Divide(nomorphhistZ)

    mergedhist_ratio = morphhist.Clone("merged_hist")
    mergedhist_ratio.Divide(nomorphhist)

    Zratio = createRatio(nomorphhistZ,morphhistZ)
    Z_mergedratio = createRatio(nomorphhist_mergedZ,morphhist_mergedZ)
    Z_mergedratio_ratio = createRatio(nomorphhist_ratio,morphhist_ratio)
#    Zratio = createRatio(morphhistZ,nomorphhistZ)
#    Z_mergedratio = createRatio(morphhist_mergedZ,nomorphhist_mergedZ)
#    Z_mergedratio_ratio = createRatio(morphhist_ratio,nomorphhist_ratio)

    c, pad1, pad2 = createCanvasPads("c")
#    l = ROOT.TLegend(0.7,0.7,0.9,0.9)
    #morphhistZ.SetTitle(hist.replace("h2_Cluster_Z_vs_phi_","")+" Morphed")
    morphhistZ.SetTitle(hist.replace("h2_Cluster_Z_vs_phi_",""))
    morphhistZ.SetLineColor(ROOT.kRed)
    morphhistZ.SetLineWidth(3)

    nomorphhistZ.SetTitle(hist.replace("h2_Cluster_Z_vs_phi_","")+" w/o Morphing Z projection")
    nomorphhistZ.SetLineWidth(3)
    nomorphhistZ.SetLineColor(ROOT.kBlue)

    morphhist_ratio.SetTitle(hist.replace("h2_Cluster_Z_vs_phi_",""))
    morphhist_ratio.SetLineColor(ROOT.kRed)
    morphhist_ratio.SetLineWidth(3)

    nomorphhist_ratio.SetTitle(hist.replace("h2_Cluster_Z_vs_phi_","")+" w/o Morphing Z projection")
    nomorphhist_ratio.SetLineWidth(3)
    nomorphhist_ratio.SetLineColor(ROOT.kBlue)

    morphhist_mergedZ.SetTitle(hist.replace("h2_Cluster_Z_vs_phi_",""))
    morphhist_mergedZ.SetLineColor(ROOT.kRed)
    morphhist_mergedZ.SetLineWidth(3)

    nomorphhist_mergedZ.SetTitle(hist.replace("h2_Cluster_Z_vs_phi_","")+" w/o Morphing Z projection")
    nomorphhist_mergedZ.SetLineWidth(3)
    nomorphhist_mergedZ.SetLineColor(ROOT.kBlue)

    pad1.cd()
    morphhistZ.Draw("hist")
    nomorphhistZ.Draw("histsame")
    l.Draw()
    pad2.cd()
    Zratio.Draw("EP")
    c.SaveAs(hist+"_Zprojection.png")

    pad1.cd()
    morphhist_mergedZ.Draw("hist")
    nomorphhist_mergedZ.Draw("histsame")
    l.Draw()
    pad2.cd()
    Z_mergedratio.Draw("EP")
    c.SaveAs(hist+"_merged_Zprojection.png")

    pad1.cd()
    morphhist_ratio.Draw("hist")
    nomorphhist_ratio.Draw("histsame")
    l.Draw()
    pad2.cd()
    Z_mergedratio_ratio.Draw("EP")
    c.SaveAs(hist+"_ratio_Zprojection.png")
