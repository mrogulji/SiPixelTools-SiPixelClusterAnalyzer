# define commandline arguments
from optparse import OptionParser

usage = "Usage: python %prog [options] \n\nExample: python %prog [--maxEvents=100 --reportEvery=1 --inputFile=cluster_tree.root]"

parser = OptionParser(usage=usage)

parser.add_option('--maxEvents', type='int', action='store',
                  default=-1,
                  dest='maxEvents',
                  help='Number of events to process. -1 means all events')

parser.add_option('--reportEvery', type='int', action='store',
                  default=100,
                  dest='reportEvery',
                  help='Report every N events (default value is 100)')

parser.add_option('--inputFile', type='string', action='store',
                  default='cluster_tree.root',
                  dest='inputFile',
                  help='Input file (default value is \'cluster_tree.root\')')

parser.add_option('--outputFile', type='string', action='store',
                  default='cluster_histograms.root',
                  dest='outputFile',
                  help='Output file (default value is \'cluster_histograms.root\')')

parser.add_option("--clusters", dest="clusters", action='store_true',
                  help="Print out clusters",
                  default=False)

parser.add_option("--pixels", dest="pixels", action='store_true',
                  help="Print out pixels",
                  default=False)

parser.add_option("--jets", dest="jets", action='store_true',
                  help="Print out jets",
                  default=False)

parser.add_option("--vtx", dest="vtx", action='store_true',
                  help="Print out the primary vertex",
                  default=False)

(options, args) = parser.parse_args()

# import ROOT
import ROOT

# configure ROOT's behavior
ROOT.gROOT.SetBatch()
ROOT.gStyle.SetOptStat("nemruoi")
ROOT.gROOT.ForceStyle()

# open input file
fIn = ROOT.TFile(options.inputFile, "READ")
# get pointer to the ROOT tree
t = fIn.Get("siPixelClusterAnalyzer/t_clusters")

# create output file
fOut = ROOT.TFile(options.outputFile, "RECREATE")

# define histograms
h_nClusters = ROOT.TH1F("h_nClusters", ";Pixel cluster multiplicity;", 2000, 0., 20000.)
h_nJets = ROOT.TH1F("h_nJets", "Jet p_{T}>20.0 GeV;Jet multiplicity;", 50, 0., 50.);
h_nTracks = ROOT.TH1F("h_nTracks", "# of Tracks;Track multiplicity;", 2000, 0., 20000.);

# total number of events
nEntries = t.GetEntries()
# loop over events
for e in range(0, nEntries):
    if options.maxEvents > 0 and (e+1) > options.maxEvents :
        break
    if e % options.reportEvery == 0 :
        print ('Event: %i' % (e+1) )

    # get event
    t.GetEntry(e)

    # fill histograms
    h_nClusters.Fill(t.clus_size.size())
    h_nJets.Fill(t.jet_px.size())
    h_nTracks.Fill(t.track_Id.size())

    # loop over clusters
    if options.clusters:
        for c in range(0, t.clus_detId.size()):
            print ("cluster %i size: %i" %(c, t.clus_size[c]) )
            # loop over cluster pixels
            if options.pixels:
                for p in range(0, t.clus_pixX[c].size()):
                    print (">> pixel %i (x, y, adc): (%i, %i, %i)" %(p, t.clus_pixX[c][p], t.clus_pixY[c][p], t.clus_pixADC[c][p]) )

    # loop over jets
    if options.jets:
        for j in range(0, t.jet_px.size()):
            print ("jet %i (px, py, pz, e): (%f, %f, %f, %f)" %(j, t.jet_px[j], t.jet_py[j], t.jet_pz[j], t.jet_e[j]) )

    # print vertex info
    if options.vtx:
        print ("vertex (x, y, z): (%f, %f, %f)" %(t.vtx_x, t.vtx_y, t.vtx_z) )

h_nClusters.Write()
h_nJets.Write()
h_nTracks.Write()

fOut.Close()
