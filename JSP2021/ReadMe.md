# Running Jupyter notebooks

After initializing a CMSSW environment as described [here](../ReadMe.md), you can start a Jupyter notebook server by running

```
root --notebook
```

However, problems have been observed with this setup when switching between different versions on CMSSW. If such problems occur, an alternative is to use ROOT from the LCG software stack. The steps to follow are the following ones (make sure not to initialize a CMSSW environment using `cmsenv` prior to executing the lines below)

```
source /cvmfs/sft.cern.ch/lcg/views/LCG_99cuda/x86_64-centos7-gcc8-opt/setup.sh
root --notebook
```
