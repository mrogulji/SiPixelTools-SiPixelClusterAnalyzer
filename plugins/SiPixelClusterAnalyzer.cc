// -*- C++ -*-
//
// Package:    SiPixelTools/SiPixelClusterAnalyzer
// Class:      SiPixelClusterAnalyzer
//
/**\class SiPixelClusterAnalyzer SiPixelClusterAnalyzer.cc SiPixelTools/SiPixelClusterAnalyzer/plugins/SiPixelClusterAnalyzer.cc

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  Dinko Ferencek
//         Created:  Wed, 26 May 2021 20:31:04 GMT
//
//


// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/InputTag.h"
#include "DataFormats/SiPixelDigi/interface/PixelDigi.h"
#include "DataFormats/SiPixelCluster/interface/SiPixelCluster.h"
#include "DataFormats/Common/interface/DetSetVector.h"
#include "SimDataFormats/TrackerDigiSimLink/interface/PixelDigiSimLink.h"
#include "DataFormats/VertexReco/interface/VertexFwd.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "DataFormats/Candidate/interface/Candidate.h"
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/SiPixelDetId/interface/PixelChannelIdentifier.h"

#ifdef CMSSW_12_0_X
// -- Start: For CMSSW_X_Y_Z>=CMSSW_11_1_0
#include "Geometry/Records/interface/TrackerTopologyRcd.h"
#include "DataFormats/TrackerCommon/interface/TrackerTopology.h"
#include "Geometry/Records/interface/TrackerDigiGeometryRecord.h"
#include "Geometry/TrackerGeometryBuilder/interface/TrackerGeometry.h"
#include "CondFormats/DataRecord/interface/SiPixelFedCablingMapRcd.h"
#include "CondFormats/SiPixelObjects/interface/SiPixelFedCablingMap.h"
// -- End: For CMSSW_X_Y_Z>=CMSSW_11_1_0
#endif
#include "Geometry/Records/interface/GlobalTrackingGeometryRecord.h"
#include "Geometry/CommonDetUnit/interface/GlobalTrackingGeometry.h"
#include "RecoLocalTracker/Records/interface/TkPixelCPERecord.h"
#include "RecoLocalTracker/ClusterParameterEstimator/interface/PixelClusterParameterEstimator.h"

#include "DQM/SiPixelPhase1Common/interface/SiPixelCoordinates.h"

#include "SiPixelTools/SiPixelClusterAnalyzer/interface/SiPixelDigiPacking.h"

#include "TTree.h"
#include "TString.h"

//
// class declaration
//

// If the analyzer does not use TFileService, please remove
// the template argument to the base class so the class inherits
// from  edm::one::EDAnalyzer<>
// This will improve performance in multithreaded jobs.

class SiPixelClusterAnalyzer : public edm::one::EDAnalyzer<edm::one::SharedResources>  {
  public:
    explicit SiPixelClusterAnalyzer(const edm::ParameterSet&);
    ~SiPixelClusterAnalyzer();

    static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

  private:
    virtual void beginJob() override;
    virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
    virtual void endJob() override;

    // ----------member data ---------------------------
    const edm::EDGetTokenT<edmNew::DetSetVector<SiPixelCluster> > clusterToken_;
    const edm::EDGetTokenT<edm::DetSetVector<PixelDigiSimLink> > digiSimLinkToken_;
    const edm::EDGetTokenT<edm::View<reco::Candidate> > jetToken_;
    const edm::EDGetTokenT<edm::View<reco::Candidate> > tauToken_;
    const edm::EDGetTokenT<reco::VertexCollection> vertexToken_;

#ifdef CMSSW_12_0_X
    // -- Start: For CMSSW_X_Y_Z>=CMSSW_11_1_0
    const edm::ESGetToken<TrackerTopology, TrackerTopologyRcd> trackerTopoToken_;
    const edm::ESGetToken<TrackerGeometry, TrackerDigiGeometryRecord> trackerGeomToken_;
    const edm::ESGetToken<SiPixelFedCablingMap, SiPixelFedCablingMapRcd> cablingMapToken_;
    // -- End: For CMSSW_X_Y_Z>=CMSSW_11_1_0
#endif
    const edm::ESGetToken<GlobalTrackingGeometry, GlobalTrackingGeometryRecord> trackingGeomToken_;
    const edm::ESGetToken<PixelClusterParameterEstimator, TkPixelCPERecord> pixelCPEToken_;

    const double ptMin_;
    const double dRMin_;
    const double dRMin2_;

    const bool addPixels_;
    const bool verbose_;

    SiPixelCoordinates coord_;

    TH1F *h_nClusters;
    TH1F *h_nJets;

    TTree *t_clusters;

    // cluster variables
    std::vector<unsigned int> clus_detId;
    std::vector<int> clus_charge;
    std::vector<unsigned int> clus_ntracks;
    std::vector<unsigned int> clus_subdet;
    std::vector<unsigned int> clus_lyrdsk;
    std::vector<unsigned int> clus_size;
    std::vector<unsigned int> clus_sizeX;
    std::vector<unsigned int> clus_sizeY;
    std::vector<std::vector<unsigned int>> clus_pixX;
    std::vector<std::vector<unsigned int>> clus_pixY;
    std::vector<std::vector<unsigned int>> clus_pixADC;
    std::vector<float> clus_glX;
    std::vector<float> clus_glY;
    std::vector<float> clus_glZ;
    // jet variables
    std::vector<float> jet_px;
    std::vector<float> jet_py;
    std::vector<float> jet_pz;
    std::vector<float> jet_e;
    // vertex variables
    float vtx_x;
    float vtx_y;
    float vtx_z;
    // track variables
    std::vector<unsigned int> track_Id;
    std::vector<int> track_bx;
    std::vector<int> track_evtId;
    std::vector<int> track_clusId;
};

//
// constants, enums and typedefs
//

//
// static data member definitions
//

//
// constructors and destructor
//
SiPixelClusterAnalyzer::SiPixelClusterAnalyzer(const edm::ParameterSet& iConfig)
 :
  clusterToken_(consumes<edmNew::DetSetVector<SiPixelCluster>>(iConfig.getParameter<edm::InputTag>("clusters"))),
  digiSimLinkToken_(consumes<edm::DetSetVector<PixelDigiSimLink>>(iConfig.getParameter<edm::InputTag>("digiSimLinks"))),
  jetToken_(consumes<edm::View<reco::Candidate> >(iConfig.getParameter<edm::InputTag>("jets"))),
  tauToken_(consumes<edm::View<reco::Candidate> >(iConfig.getParameter<edm::InputTag>("taus"))),
  vertexToken_(consumes<reco::VertexCollection>(iConfig.getParameter<edm::InputTag>("vertices"))),

#ifdef CMSSW_12_0_X
  // -- Start: For CMSSW_X_Y_Z>=CMSSW_11_1_0
  trackerTopoToken_(esConsumes<TrackerTopology, TrackerTopologyRcd>()),
  trackerGeomToken_(esConsumes<TrackerGeometry, TrackerDigiGeometryRecord>()),
  cablingMapToken_(esConsumes<SiPixelFedCablingMap, SiPixelFedCablingMapRcd>()),
  // -- End: For CMSSW_X_Y_Z>=CMSSW_11_1_0
#endif
  trackingGeomToken_(esConsumes<GlobalTrackingGeometry, GlobalTrackingGeometryRecord>()),
  pixelCPEToken_(esConsumes<PixelClusterParameterEstimator, TkPixelCPERecord>(edm::ESInputTag("", iConfig.getParameter<std::string>("pixelCPE")))),

  ptMin_(iConfig.getParameter<double>("ptMin")),
  dRMin_(iConfig.getParameter<double>("dRMin")),
  dRMin2_(dRMin_ * dRMin_),

  addPixels_(iConfig.getParameter<bool>("addPixels")),
  verbose_(iConfig.getUntrackedParameter<bool>("verbose", false))

{
  // now do what ever initialization is needed
  edm::Service<TFileService> fs;

  // define histograms
  //h_nClusters = fs->make<TH1F>("h_nClusters", ";Pixel cluster multiplicity;", 20000, 0., 20000.);
  //h_nJets = fs->make<TH1F>("h_nJets", TString::Format("Jet p_{T}>%.1f GeV;Jet multiplicity;", ptMin_).Data(), 50, 0., 50.);

  // define tree
  t_clusters = fs->make<TTree>("t_clusters", "Pixel clusters");
  // define the tree branches
  // -- cluster branches
  t_clusters->Branch("clus_detId", &clus_detId);
  t_clusters->Branch("clus_charge", &clus_charge);
  t_clusters->Branch("clus_ntracks", &clus_ntracks);
  t_clusters->Branch("clus_subdet", &clus_subdet);
  t_clusters->Branch("clus_lyrdsk", &clus_lyrdsk);
  t_clusters->Branch("clus_size", &clus_size);
  t_clusters->Branch("clus_sizeX", &clus_sizeX);
  t_clusters->Branch("clus_sizeY", &clus_sizeY);
  if (addPixels_) {
      t_clusters->Branch("clus_pixX", &clus_pixX);
      t_clusters->Branch("clus_pixY", &clus_pixY);
      t_clusters->Branch("clus_pixADC", &clus_pixADC);
  }
  t_clusters->Branch("clus_glX", &clus_glX);
  t_clusters->Branch("clus_glY", &clus_glY);
  t_clusters->Branch("clus_glZ", &clus_glZ);
  // -- jet branches
  t_clusters->Branch("jet_px", &jet_px);
  t_clusters->Branch("jet_py", &jet_py);
  t_clusters->Branch("jet_pz", &jet_pz);
  t_clusters->Branch("jet_e",  &jet_e);
  // -- vertex branches
  t_clusters->Branch("vtx_x", &vtx_x);
  t_clusters->Branch("vtx_y", &vtx_y);
  t_clusters->Branch("vtx_z", &vtx_z);
  // -- Track branches
  t_clusters->Branch("track_Id", &track_Id);
  t_clusters->Branch("track_bx", &track_bx);
  t_clusters->Branch("track_evtId", &track_evtId);
  t_clusters->Branch("track_clusId", &track_clusId);

  const size_t CLUS_SIZE = 15000;
  const size_t TRK_SIZE = 15000;
  const size_t JET_SIZE = 50;

  // reserve memory for vectors
  clus_detId.reserve(CLUS_SIZE);
  clus_charge.reserve(CLUS_SIZE);
  clus_ntracks.reserve(CLUS_SIZE);
  clus_subdet.reserve(CLUS_SIZE);
  clus_lyrdsk.reserve(CLUS_SIZE);
  clus_size.reserve(CLUS_SIZE);
  clus_sizeX.reserve(CLUS_SIZE);
  clus_sizeY.reserve(CLUS_SIZE);
  if (addPixels_) {
      clus_pixX.reserve(CLUS_SIZE);
      clus_pixY.reserve(CLUS_SIZE);
      clus_pixADC.reserve(CLUS_SIZE);
  }
  clus_glX.reserve(CLUS_SIZE);
  clus_glY.reserve(CLUS_SIZE);
  clus_glZ.reserve(CLUS_SIZE);
  jet_px.reserve(JET_SIZE);
  jet_py.reserve(JET_SIZE);
  jet_pz.reserve(JET_SIZE);
  jet_e.reserve(JET_SIZE);
  track_Id.reserve(TRK_SIZE);
  track_bx.reserve(TRK_SIZE);
  track_evtId.reserve(TRK_SIZE);
  track_clusId.reserve(TRK_SIZE);
}


SiPixelClusterAnalyzer::~SiPixelClusterAnalyzer()
{

  // do anything here that needs to be done at desctruction time
  // (e.g. close files, deallocate resources etc.)

}


//
// member functions
//

// ------------ method called for each event  ------------
void
SiPixelClusterAnalyzer::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
  using namespace edm;

  // counters
  int nClusters = 0;
  //int nJets = 0;

  // reset vectors but preserve their capacity (set using the reserve(...) method)
  clus_detId.resize(0);
  clus_charge.resize(0);
  clus_ntracks.resize(0);
  clus_subdet.resize(0);
  clus_lyrdsk.resize(0);
  clus_size.resize(0);
  clus_sizeX.resize(0);
  clus_sizeY.resize(0);
  if (addPixels_) {
      clus_pixX.resize(0);
      clus_pixY.resize(0);
      clus_pixADC.resize(0);
  }
  clus_glX.resize(0);
  clus_glY.resize(0);
  clus_glZ.resize(0);
  jet_px.resize(0);
  jet_py.resize(0);
  jet_pz.resize(0);
  jet_e.resize(0);
  track_Id.resize(0);
  track_bx.resize(0);
  track_evtId.resize(0);
  track_clusId.resize(0);

#ifdef CMSSW_12_0_X
  // -- Start: For CMSSW_X_Y_Z>=CMSSW_11_1_0
  // get tracker topology
  edm::ESHandle<TrackerTopology> trackerTopo = iSetup.getHandle(trackerTopoToken_);

  // get tracker geometry
  edm::ESHandle<TrackerGeometry> trackerGeom = iSetup.getHandle(trackerGeomToken_);

  // get pixel cabling map
  edm::ESHandle<SiPixelFedCablingMap> cablingMap = iSetup.getHandle(cablingMapToken_);
  // -- End: For CMSSW_X_Y_Z>=CMSSW_11_1_0
#endif

  // initialize the SiPixelCoordinates tool
#ifdef CMSSW_12_0_X
  coord_.init(trackerTopo.product(), trackerGeom.product(), cablingMap.product()); // For CMSSW_X_Y_Z>=CMSSW_11_1_0
#else
  coord_.init(iSetup);
#endif

  // get tracking geometry
  edm::ESHandle<GlobalTrackingGeometry> geometry = iSetup.getHandle(trackingGeomToken_);

  // get pixel CPE
  edm::ESHandle<PixelClusterParameterEstimator> cpe = iSetup.getHandle(pixelCPEToken_);

  // get input pixel clusters
  edm::Handle<edmNew::DetSetVector<SiPixelCluster> > clusters;
  iEvent.getByToken(clusterToken_, clusters);

  // get input PixelDigiSimLinks
  edm::Handle<edm::DetSetVector<PixelDigiSimLink> > digiSimLinks;
  iEvent.getByToken(digiSimLinkToken_, digiSimLinks);

  // PixelDigiSimLink iterators
  edm::DetSetVector<PixelDigiSimLink>::const_iterator dSLDetIdIter;
  const edm::DetSetVector<PixelDigiSimLink>::const_iterator dSLDetIdEndIter = digiSimLinks->end();

  // loop over DetUnits
  edmNew::DetSetVector<SiPixelCluster>::const_iterator clusDetIdIter = clusters->begin();
  for( ; clusDetIdIter != clusters->end(); ++clusDetIdIter)
  {
    auto id = clusDetIdIter->detId();

    const GeomDetUnit* geomDetUnit = geometry->idToDetUnit(id);

    if(verbose_) { std::cout << "DetId: " << id << std::endl; }

    const DetId detId(id);
    const unsigned int subdetId = detId.subdetId();
    const unsigned int subdet = subdetId - 1;
    const int lyrdsk = ( (subdetId == PixelSubdetector::PixelBarrel) ? coord_.layer(detId) : coord_.disk(detId) );

    // cluster iterators
    edmNew::DetSet<SiPixelCluster>::const_iterator clusIter;
    const edmNew::DetSet<SiPixelCluster>::const_iterator clusBeginIter = clusDetIdIter->begin();
    const edmNew::DetSet<SiPixelCluster>::const_iterator clusEndIter = clusDetIdIter->end();

    // fetch PixelDigiSimLinks from the same DetUnit
    dSLDetIdIter = digiSimLinks->find(id);
    bool digiSimLinksFound = false;
    if (dSLDetIdIter != dSLDetIdEndIter) digiSimLinksFound = true;

    // loop over clusters in a given DetUnit
    for(clusIter = clusBeginIter; clusIter != clusEndIter; ++clusIter)
    {
      if(verbose_) { std::cout << ">> Cluster: " << (clusIter - clusBeginIter) << std::endl; }

      LocalPoint clustLocalCoordinates;
      std::tie(clustLocalCoordinates, std::ignore, std::ignore) = cpe->getParameters(*clusIter, *geomDetUnit);
      GlobalPoint clustGlobalCoordinates = geomDetUnit->toGlobal(clustLocalCoordinates);

      clus_detId.push_back(id);
      clus_charge.push_back(clusIter->charge());
      clus_subdet.push_back(subdet);
      clus_lyrdsk.push_back(lyrdsk);
      clus_size.push_back(clusIter->size());
      clus_sizeX.push_back(clusIter->sizeX());
      clus_sizeY.push_back(clusIter->sizeY());
      clus_glX.push_back(clustGlobalCoordinates.x());
      clus_glY.push_back(clustGlobalCoordinates.y());
      clus_glZ.push_back(clustGlobalCoordinates.z());

      // std::set to count the number of distinct tracks in a cluster
      std::set<std::tuple<unsigned int,int,int>> trk_info;

      std::vector<unsigned int> pixX;
      std::vector<unsigned int> pixY;
      std::vector<unsigned int> pixADC;
      if (addPixels_) {
          pixX.reserve(clusIter->size());
          pixY.reserve(clusIter->size());
          pixADC.reserve(clusIter->size());
      }

      // loop over pixels in a given cluster
      for(auto i = 0; i < clusIter->size(); ++i)
      {
        const SiPixelCluster::Pixel pixel = clusIter->pixel(i);

        // get pixel coordinates
        auto x = pixel.x;
        auto y = pixel.y;
        auto adc = pixel.adc;
        if (addPixels_) {
            pixX.push_back(x);
            pixY.push_back(y);
            pixADC.push_back(adc);
        }
        if(verbose_) { std::cout << ">>>> Pixel i x y adc: " << i << " " << x << " " << y << " " << adc << std::endl; }

        // try to find the corresponding PixelDigiSimLink
        if(digiSimLinksFound)
        {
            // PixelDigiSimLink iterators
            edm::DetSet<PixelDigiSimLink>::const_iterator dSLIter;
            const edm::DetSet<PixelDigiSimLink>::const_iterator dSLBeginIter = dSLDetIdIter->begin();
            const edm::DetSet<PixelDigiSimLink>::const_iterator dSLEndIter = dSLDetIdIter->end();

            // loop over PixelDigiSimLinks in a given DetUnit
            for(dSLIter = dSLBeginIter; dSLIter != dSLEndIter; ++dSLIter)
            {
                std::pair<int, int> pix = SiPixelDigiPacking::channelToPixel(dSLIter->channel()); // using custom decoding function to be able to read 10_6_X digis in 12_0_X
                // if found the same pixel
                if(x==pix.first && y==pix.second)
                {
                  if(verbose_) { std::cout << ">>>>>> TrackId BX Event: " << dSLIter->SimTrackId() << " " << dSLIter->eventId().bunchCrossing() << " " << dSLIter->eventId().event() << std::endl; }

                  trk_info.insert( std::make_tuple(dSLIter->SimTrackId(),dSLIter->eventId().bunchCrossing(),dSLIter->eventId().event()) );
                }
            }
        }
      }

      if (addPixels_) {
          clus_pixX.push_back(pixX);
          clus_pixY.push_back(pixY);
          clus_pixADC.push_back(pixADC);
      }

      if(verbose_){std::cout<<"# of tracks in the cluster:  "<<trk_info.size()<<std::endl;}

      clus_ntracks.push_back(trk_info.size());

      auto trk_iter = trk_info.cbegin();
      for(; trk_iter!=trk_info.cend();++trk_iter)
      {
          const auto &trk = *trk_iter;
          track_Id.push_back(std::get<0>(trk));
          track_bx.push_back(std::get<1>(trk));
          track_evtId.push_back(std::get<2>(trk));
          track_clusId.push_back(nClusters);
      }

      // increment the cluster counter
      nClusters++;
    }
  }

  // get input taus
  edm::Handle<edm::View<reco::Candidate> > taus;
  iEvent.getByToken(tauToken_, taus);

  // get input jets
  edm::Handle<edm::View<reco::Candidate> > jets;
  iEvent.getByToken(jetToken_, jets);

  // loop over jets
  for(const auto& jet : (*jets))
  {
      if (jet.pt() < ptMin_) continue;

      bool shouldVeto = false;
      // loop over taus to apply a dR veto
      for(const auto& tau : (*taus))
      {
          if( reco::deltaR2(jet.eta(), jet.phi(), tau.eta(), tau.phi()) < dRMin2_ )
          {
              shouldVeto = true;
              break;
          }
      }
      if(shouldVeto) continue;

      jet_px.push_back(jet.px());
      jet_py.push_back(jet.py());
      jet_pz.push_back(jet.pz());
      jet_e.push_back(jet.energy());
      //nJets++;
  }

  // get the primary vertex
  edm::Handle<reco::VertexCollection> vertices;
  iEvent.getByToken(vertexToken_, vertices);
  const reco::Vertex& pv = (*vertices)[0];

  vtx_x = pv.x();
  vtx_y = pv.y();
  vtx_z = pv.z();

  // fill histograms
  //h_nClusters->Fill(nClusters);
  //h_nJets->Fill(nJets);

  // fill the tree
  t_clusters->Fill();
}


// ------------ method called once each job just before starting event loop  ------------
void
SiPixelClusterAnalyzer::beginJob()
{
}

// ------------ method called once each job just after ending the event loop  ------------
void
SiPixelClusterAnalyzer::endJob()
{
}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void
SiPixelClusterAnalyzer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(SiPixelClusterAnalyzer);
